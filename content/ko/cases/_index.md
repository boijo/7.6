---
title: 적용 사례
linkTitle: 적용 사례
layout: docs
---

{{% blocks/cover title="적용 사례" height="auto" %}}

 로그 통합 관리 시스템은 여러 시스템들을 운영해야되는 상황에서 정말 유용한 시스템입니다.

 로그 통합 관리 시스템을 적용한 사례는 다음과 같습니다.
{{% /blocks/cover %}}




